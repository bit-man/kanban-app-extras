package com.bitman.kanbanapp.extras.cleanup;

import com.metservice.kanban.KanbanService;
import com.metservice.kanban.model.KanbanProject;
import com.metservice.kanban.model.WorkItem;
import org.joda.time.LocalDate;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

import static com.metservice.kanban.utils.DateUtils.currentLocalDate;

/**
 * Description : Cleanup
 * Date: 14-Jul-2014
 * Time: 3:11 PM
 */
public class Cleaner {
    private final File destHome;
    private final String projectNameSrc;
    private final String projectNameDest;
    private final File srcHome;

    public Cleaner(File srcHome, File destHome, String projectName) {
        this(srcHome, destHome, projectName, projectName);
    }

    public Cleaner(File srcHome, File destHome, String projectNameSrc, String projectNameDest) {
        this.srcHome = srcHome;
        this.destHome = destHome;
        this.projectNameSrc = projectNameSrc;
        this.projectNameDest = projectNameDest;
    }

    public void doClean(PrintStream out) throws IOException {
        KanbanService srcSvc = new KanbanService(srcHome);
        KanbanService destSvc = new KanbanService(destHome);

        // Locate the desired project
        final KanbanProject srcSvcKanbanProject = srcSvc.getKanbanProject(projectNameSrc);
        final KanbanProject destProject = destSvc.getKanbanProject(projectNameDest);

        for( WorkItem wi : srcSvcKanbanProject.getWorkItemTree().getWorkItemList() ) {
            // Process only completed workitems
            if (wi.isCompleted()) {
                // Move to new project
                out.println("Processing " + wi.toString());

                int id = destProject.addWorkItem(wi.getParentId(), destProject.getWorkItemTypes().getByName(wi.getType().getName()), wi.getName(), wi.getAverageCaseEstimate(), wi.getWorstCaseEstimate(),
                        wi.getImportance(), wi.getNotes(), wi.getColour().toString(), wi.isExcluded(), wi.getWorkStreamsAsString(), currentLocalDate());

                WorkItem workItem = destProject.getWorkItemById(id);
                workItem.setWorkStreamsAsString(wi.getWorkStreamsAsString());
                final Map<String, LocalDate> datesByPhase = wi.getDatesByPhase();
                for ( String phase : datesByPhase.keySet() ) {
                    workItem.setDate(phase, datesByPhase.get(phase));
                }
                workItem.resetCommentsAndReplaceWith(wi.getComments());

                srcSvcKanbanProject.deleteWorkItem(wi.getId());

                // ToDo not ACID. Should it ?
                destProject.save();
                srcSvcKanbanProject.save();

                // ToDo per workitem or per run save ?
            }

        }
    }
}
