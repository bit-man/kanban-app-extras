package com.bitman.kanbanapp.extras.cleanup;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class Main {

    public static void main( String[] arg) throws IOException {

        final PrintStream out = System.out;

        // ToDo add logging
        out.println("Cleanup START...");
        Cleaner cleaner = null;

        // ToDo better arg parsing
        if ( arg.length == 3 ) {
            cleaner = new Cleaner(new File(arg[0]), new File(arg[1]), arg[2]);
        } else if (arg.length == 4) {
            cleaner = new Cleaner(new File(arg[0]), new File(arg[1]), arg[2], arg[3]);
        } else {
            usage(out);
            System.exit(-1);
        }

        cleaner.doClean(out);
        out.println("Cleanup END");
    }

    private static void usage(PrintStream out) {
        out.print(  "\n" +
                    "usage : \n" +
                    "\t" + Main.class.getName() + " srcHomePath destHomePath projectName [projectNameDest]\n" +
                    "\n");
    }
}

