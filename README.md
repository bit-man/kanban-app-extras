kanban-app-extras
=================

Extra tools for [Metaservice Kanban App](https://github.com/kanban/kanban-app).

Build
=====

Build status : ![build status](https://travis-ci.org/bit-man/kanban-app-extras.svg)

Simply do a *mvn package* and the JAR for this project will be available at *target/kanban-app-extras-0.0-SNAPSHOT.jar*

Kanban App jar
==============

The Kanban App classes are no available in a JAR container but embedded into the WAR needed to be deployed into a 
servlet container. Just to make kanban-app-extras work those classes were extracted fomr kanan-app WAR and placed into lib/kanban.jar

Enjoy !
